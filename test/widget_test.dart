// AUTOMATIC TESTS

import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';

import 'package:mobile_develop/main.dart';

void main() {
  testWidgets('Counter increments smoke test', (WidgetTester tester) async {
    // BUILD OUR APP AND TRIGGER A FRAME
    await tester.pumpWidget(MobileDevelopApp());

    // VERIFY THAT OUR COUNTER STARTS AT 0
    expect(find.text('0'), findsOneWidget);
    expect(find.text('1'), findsNothing);

    // TAP THE '+' ICON AND TRIGGER A FRAME
    await tester.tap(find.byIcon(Icons.add));
    await tester.pump();

    // VERIFY THAT OUR COUNTER HAS INCREMENTED
    expect(find.text('0'), findsNothing);
    expect(find.text('1'), findsOneWidget);
  });
}
