import 'dart:async';

import 'package:flutter/material.dart';

class CustomWidget extends StatefulWidget {
  CustomWidget({Key key}) : super(key: key);

  @override
  _CustomWidgetState createState() => _CustomWidgetState();
}

class _CustomWidgetState extends State<CustomWidget> {
  String title = '';

  @override
  Widget build(BuildContext context) {
    return Row(
      children: <Widget>[
        Text(title),
      ],
    );
  }
}
