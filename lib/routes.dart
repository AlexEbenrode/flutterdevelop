import 'package:flutter/material.dart';
import 'package:mobile_develop/pages/page_main.dart';

class RoutesApp {
  static var routes = <String, WidgetBuilder>{
    'main': (BuildContext context) => new MainPage(),
  };
}
