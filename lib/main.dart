import 'package:flutter/material.dart';
import 'package:mobile_develop/config/colors.dart';
import 'package:mobile_develop/config/general.dart';
import 'package:mobile_develop/routes.dart';

import 'pages/page_main.dart';

void main() => runApp(MobileDevelopApp());

class MobileDevelopApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: GeneralConfig.APP_NAME,
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        platform: TargetPlatform.android,
        accentColor: AppColors.ACCENT_COLOR,
      ),
      routes: RoutesApp.routes,
      home: MainPage(),
      // FOR ROUTES SYSTEM USING
      // initialRoute: 'main',
    );
  }
}
