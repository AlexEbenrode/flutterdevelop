import 'package:flutter/material.dart';
import 'package:mobile_develop/config/colors.dart';

class MainPage extends StatefulWidget {
  MainPage({Key key}) : super(key: key);

  final String title = 'Главная страница';

  @override
  _MainPageState createState() => _MainPageState();
}

class _MainPageState extends State<MainPage> {
  String selectedLocation;
  final textController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: AppColors.STATUS_BAR_COLOR,
        title: Row(
          children: <Widget>[Icon(Icons.laptop), Text(' ' + widget.title)],
        ),
      ),
      body: new Container(
        padding: EdgeInsets.all(10),
        child: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Image.asset(
                'assets/chip.png',
                width: 200,
                height: 200,
              )
            ],
          ),
        ),
      ),
      floatingActionButton: new Builder(builder: (BuildContext context) {
        return new FloatingActionButton(
          onPressed: () {
            Scaffold.of(context).showSnackBar(new SnackBar(
              content: new Text("ACTION SNACKBAR"),
            ));
          },
          tooltip: 'ACTION',
          child: Icon(Icons.add),
        );
      }),
    );
  }
}
