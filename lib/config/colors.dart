import 'package:flutter/material.dart';

// https://www.materialpalette.com/

class AppColors{
  static const STATUS_BAR_COLOR = Colors.deepPurple;
  static const ACCENT_COLOR = Colors.blueAccent;
}
